#!/usr/bin/env python3

# Copyright 2015-2021 Louis Paternault
#
# ChangeWallpaper is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ChangeWallpaper is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with ChangeWallpaper.  If not, see <http://www.gnu.org/licenses/>.

"""Randomly choose the Gnome3 background picture.

* If called without argument, pick an image at random among the
  WALLPAPERS directory.
* If called with arguments, pick an image at random among files given
  in arguments, or images belonging to directories given in
  arguments.
"""

import logging
import mimetypes
import os
import random
import sys

from gi.repository import Gio

logging.basicConfig(level=logging.INFO)


class Error(Exception):
    """Generic class for errors."""

    def __init__(self, message):
        super().__init__()
        self.message = message

    def __str__(self):
        return self.message


def wallpaper_directory():
    """Return wallpaper directory.

    May raise an `Error` exception.
    """
    try:
        return os.environ["WALLPAPERS"]
    except KeyError as error:
        raise Error("Please define a 'WALLPAPERS' environment variables.") from error


def iter_files(directory):
    """Iterate over subfiles of argument."""
    for root, _, files in os.walk(directory):
        for filename in files:
            yield os.path.join(root, filename)


def shuffled_files():
    """Return a shuffled list of files.

    The files are the one considered in this command argument (see
    command usage for more information).
    """
    file_set = set()

    if len(sys.argv) == 1:
        file_set.update(iter_files(wallpaper_directory()))
    else:
        for argument in sys.argv[1:]:
            if os.path.exists(argument):
                if os.path.isfile(argument):
                    file_set.add(argument)
                elif os.path.isdir(argument):
                    file_set.update(iter_files(argument))

    file_list = list(file_set)
    random.shuffle(file_list)
    return file_list


def get_random_image():
    """Get a random image"""
    files = shuffled_files()
    while files:
        candidate = files.pop()
        if mimetypes.guess_type(candidate)[0].split("/")[0] == "image":
            return candidate
    raise IndexError("No image found.")


def set_background(background):
    """Set background image"""
    logging.info("Set background to '%s'.", background)
    Gio.Settings.new("org.gnome.desktop.background").set_string(
        "picture-uri",
        "file://" + os.path.abspath(background),
    )


def main():
    """Main function."""
    try:
        set_background(get_random_image())
    except Error as error:
        logging.error(error)
        sys.exit(1)


if __name__ == "__main__":
    main()
