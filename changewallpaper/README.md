changewallpaper — Choose a random desktop wallpaper from a local repository
===========================================================================

This only works with Gnome3.

Usage
-----

~~~
$ ./changewallpaper.py [FILE or DIRECTORY]

Choose and set a random wallpaper:

- if no arguments are given, select it from the directory set in the `WALLPAPERS` environment variables;
- otherwise, select an image at random among the files given in argument, or from files in directories given in arguments.
"""
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
