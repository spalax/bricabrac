describeimages — A small GUI tool to describe images (as a .txt file along the image file)
==========================================================================================

Provide a (as much as possible) simple interface to annotate pictures with descriptions. Description of image foo.jpg is stored in foo.txt.

![Screenshot](screenshot.png)

Annotation format
-----------------

Why aren't annotations stored as EXIF or IPTC tags? [Because.](https://xkcd.com/927/)

In my opinion, EXIF or IPTC tags are hard (as in: no ergonomic way) to see and edit. On the contrary, a plain .txt file can be seen by any file manager, and read and written by any text editor.

Usage
-----

~~~
$ ./describeimages.py [FILES]
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
