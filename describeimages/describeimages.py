#!/usr/bin/env python3

# Copyright 2011-2021 Louis Paternault

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""A GUI interface to annotate pictures

Description of image foo.jpg is stored in foo.txt.

Usage: one of:
    describeimages
    describeimages FILE
    describeimages FILES

If several filenames are given:
    process every file given in argument.
If one filename is given:
    processe every file in the directory of the argument, starting with the argument.
Otherwise:
    process every file in the image directory.
"""

# pylint: disable=line-too-long

import contextlib
import logging
import mimetypes
import os
import pathlib
import sys
import textwrap

from tkinter import ttk
from tkinter import messagebox, scrolledtext
import tkinter as tk

from PIL import ImageTk, Image, ImageOps

TITLE = "Image description"
ICON = "/usr/share/icons/gnome/48x48/emblems/emblem-photos.png"


def _(text):
    return text


class MyException(Exception):
    """Exception that is meant to be nicely catched, displayed, before quitting."""


@contextlib.contextmanager
def temporarykeyvalue(dictionary, key, value):
    """Context manager to temporary set dictionary[key] to value."""
    oldvalue = dictionary[key]
    dictionary[key] = value
    yield
    dictionary[key] = oldvalue


class DescribeImages(ttk.Frame):
    """A graphical application to display images."""

    # pylint: disable=too-many-ancestors, too-many-instance-attributes

    def __init__(self, images, *, parent=None):
        super().__init__(parent)
        self.parent = parent
        if os.path.exists(ICON):
            self.parent.iconphoto(False, tk.PhotoImage(file=ICON))
        self.parent.geometry("640x480")
        self.parent.minsize(width=320, height=240)
        self.parent.title(TITLE)

        self.images = images
        self._create_widgets()
        self.open()

    def resize(self, event=None):
        """Fit image to window."""
        try:
            image = ImageOps.exif_transpose(Image.open(self.images.current))
        except OSError:
            self.image.configure(
                image=ImageTk.PhotoImage(Image.new("RGB", (1, 1), (0, 0, 0)))
            )
            raise

        image_size = image.size
        if event is None:
            available_size = (self.image.winfo_width(), self.image.winfo_height())
        else:
            available_size = (event.width, event.height)

        size = (available_size[0], (image_size[1] * available_size[0]) // image_size[0])
        if size[1] > available_size[1]:
            size = (
                (image_size[0] * available_size[1]) // image_size[1],
                available_size[1],
            )
        size = (max(1, size[0]), max(1, size[1]))

        self._resized = (  # pylint: disable=attribute-defined-outside-init
            ImageTk.PhotoImage(image.resize(size))
        )
        self.image.configure(image=self._resized)

    def replace_text(self, text):
        """Replace content of the text box with argument."""
        with temporarykeyvalue(self.text, "state", "normal"):
            self.text.delete("1.0", tk.END)
            if text:
                self.text.insert(tk.INSERT, text)
        self.text.edit_modified(False)
        self.parent.unbind("<Control-z>")
        self.parent.unbind("<Control-s>")

    def enable_text(self):
        """Enable the text box."""
        self.text["state"] = "normal"
        self.text.configure(
            font=("TkFixedFont", 14), foreground="black", background="white"
        )
        self.parent.bind("<Control-z>", self.open)
        self.parent.bind("<Control-s>", self.save)

    def disable_text(self):
        """Disable the text box."""
        self.text["state"] = "disabled"
        self.text.configure(
            font=("TkFixedFont", 14, "italic"),
            foreground="black",
            background="lightgray",
        )
        self.parent.unbind("<Control-z>")
        self.parent.unbind("<Control-s>")

    def write(self, *, ask):
        """Write description to file.

        :param bool ask: Ask before overwriting.
        :return: `True` if success, `False` description was not written.
        """
        if (
            self.text["state"] == "disabled"
            or (
                self.original_description is None
                and self.text.get("1.0", "end-1c") == ""
            )
            or self.text.get("1.0", "end-1c") == self.original_description
        ):
            return True

        if ask:
            if not messagebox.askokcancel(
                title=_("Save ?"), message=_("Do you want to save the description?")
            ):
                return False

        try:
            if self.text.get("1.0", "end-1c"):
                with open(
                    self.images.description, mode="w"
                ) as description:  # pylint: disable=unspecified-encoding
                    description.write(self.text.get("1.0", "end-1c"))
            else:
                self.images.description.unlink(missing_ok=True)
        except Exception as error:  # pylint: disable=broad-except
            logging.error(error)
            messagebox.showerror(
                _("Error"),
                message=textwrap.dedent(
                    _(
                        f"""\
                    An error happened while writing to "{self.images.description}": {error}.

                    Either fix the error, or cancel your changes.
                    """
                    )
                ),
            )
            return False

        return True

    def open(self, *args, **kwargs):
        """Open image: display and resize it, open and show description."""
        # pylint: disable=unused-argument
        try:
            self.resize()
        except OSError as error:
            logging.error('Could not read image "%s": %s.', self.images.current, error)
            self.original_description = None
            self.replace_text(
                textwrap.dedent(
                    _(
                        f"""\
                    Could not read image "{self.images.current}": {error}.
                    Try changing permissions of, or deleting, "{self.images.current}".

                    Editing the description of this image is disabled until this problem has been fixed.
                    """
                    )
                )
            )
            self.disable_text()
        else:
            try:
                with open(
                    self.images.description, mode="r"
                ) as description:  # pylint: disable=unspecified-encoding
                    self.original_description = description.read()
                self.replace_text(self.original_description)
                self.enable_text()
            except FileNotFoundError:
                self.original_description = None
                self.replace_text("")
                self.enable_text()
            except OSError as error:
                logging.error(
                    'Could not read image description "%s": %s.',
                    self.images.description,
                    error,
                )
                self.original_description = None
                self.replace_text(
                    textwrap.dedent(
                        _(
                            f"""\
                        Could not read image description: {error}.
                        Try changing permissions of "{self.images.description}", or deleting, "{self.images.current}".

                        Editing the description of this image is disabled until this problem has been fixed.
                        """
                        )
                    )
                )
                self.disable_text()

        self.btn_save["state"] = "disabled"
        self.btn_cancel["state"] = "disabled"
        self.parent.title(_(f"{TITLE} — {self.images.current}"))
        self.text.focus()

    def modified(self, event):
        """If text box has been modified, enable buttons."""
        # pylint: disable=unused-argument
        if (
            self.text["state"] == "disabled"
            or (
                self.original_description is None
                and self.text.get("1.0", "end-1c") == ""
            )
            or self.text.get("1.0", "end-1c") == self.original_description
        ):
            # Nothing changed
            return

        self.btn_save["state"] = "normal"
        self.btn_cancel["state"] = "normal"
        self.parent.title(_(f"{TITLE} — {self.images.current} (modified)"))

    def _create_widgets(self):
        self.grid(sticky=(tk.N, tk.S, tk.E, tk.W))

        # Image
        self.image = tk.Label(self)

        # Place on grid
        self.image.grid(
            column=0,
            row=0,
            columnspan=7,
            rowspan=1,
            sticky=(tk.E, tk.W, tk.N, tk.S),
        )

        self.columnconfigure(0, weight=1)
        self.columnconfigure(6, weight=1)
        self.rowconfigure(0, weight=1)
        self.parent.columnconfigure(0, weight=1)
        self.parent.rowconfigure(0, weight=1)

        # Bindings
        self.parent.bind("<Escape>", self.write_and_quit)
        self.image.bind("<Configure>", self.resize)

        # Text editor
        self.text = scrolledtext.ScrolledText(
            self,
            height=10,
        )
        self.enable_text()
        self.text.grid(
            column=0,
            row=1,
            columnspan=7,
            rowspan=1,
            sticky=(tk.E, tk.W),
        )
        self.text.focus()
        self.text.bind("<<Modified>>", self.modified)

        # Buttons
        self.btn_previous = ttk.Button(
            self,
            text=_("\N{Leftwards Black Arrow} Previous"),
            command=self.left,
        )
        self.parent.bind("<Alt-Left>", self.left)

        self.btn_help = ttk.Button(
            self, text=_("\N{question mark} Help"), command=self.help
        )
        self.parent.bind("<F1>", self.help)

        self.btn_cancel = ttk.Button(
            self,
            text=_("\N{cross mark} Cancel"),
            command=self.open,
        )

        self.btn_save = ttk.Button(
            self,
            text=_("\N{check mark} Save"),
            command=self.save,
        )

        self.btn_next = ttk.Button(
            self, text=_("Next \N{Rightwards Black Arrow}"), command=self.right
        )
        self.parent.bind("<Alt-Right>", self.right)

        # Place on grid
        self.btn_previous.grid(column=1, row=2, sticky=(tk.S), padx=5, pady=5)
        self.btn_help.grid(column=2, row=2, sticky=(tk.S), padx=5, pady=5)
        self.btn_cancel.grid(column=3, row=2, sticky=(tk.S), padx=5, pady=5)
        self.btn_save.grid(column=4, row=2, sticky=(tk.S), padx=5, pady=5)
        self.btn_next.grid(column=5, row=2, sticky=(tk.S), padx=5, pady=5)

    def write_and_quit(self, *args, **kwargs):
        """Save description, and quit."""
        # pylint: disable=unused-argument
        if self.write(ask=True):
            self.quit()

    def save(self, *args, **kwargs):
        """Save description"""
        # pylint: disable=unused-argument
        if self.write(ask=False):
            self.open()

    def left(self, *args, **kwargs):
        """Save description, and go to previous image."""
        # pylint: disable=unused-argument
        if self.write(ask=False):
            self.images.previous()
            self.open()

    def right(self, *args, **kwargs):
        """Save description, and go to next image."""
        # pylint: disable=unused-argument
        if self.write(ask=False):
            self.images.next()
            self.open()

    def help(self, *args, **kwargs):
        """Display a message box to show help."""
        # pylint: disable=unused-argument, no-self-use
        messagebox.showinfo(
            title=_("Help"),
            message=textwrap.dedent(
                """\
            Here are some shortcuts.
            - Alt-Left: save description and go to previous picture.
            - Alt-Right: save description and go to previous picture.
            - Ctrl-S: save description.
            - Ctrl-Z: cancel changes (current picture only).
            - Escape: save and quit.
            - F1: Display this help.
            """
            ),
        )

    def quit(self, *args, **kwargs):
        # pylint: disable=unused-argument
        self.parent.destroy()


################################################################################
# Basic command line parsing


def usage():
    """
    Print usage.
    """
    print(
        textwrap.dedent(
            """\
            Usage:
                %(program)s FILES
                %(program)s DIRECTORY

            Provide either (one or) several files, or one directory.

            Note: this script is not meant to be used in command line, which
            explains this poor command line help and interface.\
    """
        )
        % {"program": sys.argv[0]}
    )


################################################################################
# Iterate over images


def mime_is_image(filename):
    """
    Return True iff "filename" (as a string representing the path to a file) is
    an image, according to its mime-type.
    """
    # Mime type is of the form type/subtype. We only want the first part
    mime = mimetypes.guess_type(filename, strict=False)[0]
    if mime is None:
        return False
    return mime.split("/")[0] == "image"


def default_pictures_directory():
    """Return the default directory where to find images."""
    try:
        # pylint: disable=import-outside-toplevel
        from gi.repository import GLib

        return pathlib.Path(
            GLib.get_user_special_dir(GLib.UserDirectory.DIRECTORY_PICTURES)
        )
    except ImportError:
        return pathlib.Path.home() / "Pictures"


class ImageList:
    """List of images, with a notion of a "current" image.

    Can go to next or previous image.
    """

    @property
    def current(self):
        """Path to the current image."""
        raise NotImplementedError()

    @property
    def description(self):
        """File path of the description of the current image."""
        return self.current.parent / f"{self.current.stem}.txt"

    def next(self, *, reverse=False):
        """Set self.current to next image."""
        raise NotImplementedError()

    def previous(self):
        """Set self.current to previous image."""
        raise NotImplementedError()

    @classmethod
    def from_args(cls, args):
        """Construct an object from command line arguments.

        - Several arguments: an ImageListFixed of those arguments.
        - No argument: an ImageListDirectory of the default directory.
        - One directory argument: an ImageListDirectory of this directory.
        - One file argument: an ImageListDirectory of the directory of the argument, the current image being the argument.
        """
        if len(args) == 0:
            args = [default_pictures_directory()]
        if len(args) == 1:
            if os.path.isdir(args[0]):
                return ImageListDirectory(pathlib.Path(args[0]))
            if os.path.isfile(args[0]):
                arg = pathlib.Path(args[0])
                return ImageListDirectory(arg.parent, current=arg.name)
            raise MyException(f"No file or directory named '{args[0]}'.")
        return ImageListFixed(args)


class ImageListFixed(ImageList):
    """Hard-coded list of images.

    The list of images is defined in constructor, and is not changed after.
    """

    def __init__(self, args):
        self.images = []
        for item in args:
            if not os.path.exists(item):
                logging.warning("File '%s' does not exist. Ignored.", item)
                continue
            if not os.path.isfile(item):
                logging.warning("Path '%s' is not a file. Ignored.", item)
                continue
            if not mime_is_image(item):
                logging.warning("File '%s' is not an image. Ignored.", item)
                continue
            self.images.append(pathlib.Path(item))
        if not self.images:
            raise MyException("No valid image found.")
        self.index = 0

    @property
    def current(self):
        return self.images[self.index]

    def next(self, *, reverse=False):
        # pylint: disable=unused-argument
        self.index = (self.index + 1) % len(self.images)
        return self.current

    def previous(self):
        self.index = (self.index - 1) % len(self.images)
        return self.current


class ImageListDirectory(ImageList):
    """List of images of a directory.

    If images are added or removed, this list is updated accordingly.
    """

    def __init__(self, directory, *, current=None):
        self.directory = directory
        self.name = current

        if self.name is None:
            self.name = ""
            self.next()

    @property
    def current(self):
        return self.directory / self.name

    def next(self, *, reverse=False):
        files = [
            item.name
            for item in sorted(self.directory.iterdir(), reverse=reverse)
            if item.is_file()
        ]

        if reverse:
            candidates = (
                [name for name in files if name < self.name]
                + [name for name in files if name > self.name]
                + [self.name]
            )
        else:
            candidates = (
                [name for name in files if name > self.name]
                + [name for name in files if name < self.name]
                + [self.name]
            )

        for name in candidates:
            if mime_is_image(name):
                self.name = name
                return self.current

        return self.current

    def previous(self):
        return self.next(reverse=True)


def main():
    """Main function."""
    try:
        root = tk.Tk()
        DescribeImages(parent=root, images=ImageList.from_args(sys.argv[1:]))
        root.mainloop()
    except MyException as error:
        logging.error(error)
        sys.exit(1)


if __name__ == "__main__":
    main()
