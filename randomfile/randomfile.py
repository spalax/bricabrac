#!/usr/bin/env python3

# Copyright 2022 Louis Paternault
#
# Randomfile is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Randomfile is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with Randomfile.  If not, see <http://www.gnu.org/licenses/>.

"""Display a file chosen at random.

https://framagit.org/spalax/bricabrac/-/tree/main/randomfile
"""

import argparse
import pathlib
import random

NAME = "randomfile"
VERSION = "0.1.0"


def randomfile(path, /):
    """Return a file chosen at random.

    :param list path: List of path (files or directories).
    """
    chosen = random.choice(path)
    if chosen.is_file():
        return chosen
    return randomfile(list(chosen.iterdir()))


def parse():
    """Parse command line arguments, and returns the path."""
    parser = argparse.ArgumentParser(
        description="""Display a file, randomly chosen from the path given in argument.""",
        add_help=True,
    )

    parser.add_argument(
        "--version", action="version", version=f"{NAME} version {VERSION}"
    )

    parser.add_argument(
        "path",
        nargs="*",
        default=[pathlib.Path(".")],
        help="Location of the random file (current directory if absent).",
        type=pathlib.Path,
    )

    return parser.parse_args().path


def main():
    """Main function."""
    print(randomfile(parse()))


if __name__ == "__main__":
    main()
