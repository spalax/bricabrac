RandomFile — Display a random file
==================================

The file is chosen, randomly, among the arguments (if files) or in the arguments (if directories). Note that there is no equiprobability here.

Usage
-----

~~~
$ randomfile.py --help
usage: randomfile.py [-h] [path ...]

Display a file, randomly chosen from the path given in argument.

positional arguments:
  path                    Location of the random file (current directory if absent).

  optional arguments:
    -h, --help            show this help message and exit
    --version             show program's version number and exit
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
