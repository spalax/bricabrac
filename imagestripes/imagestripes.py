#!/usr/bin/env python3

# Copyright 2021-2024 Louis Paternault
#
# ImageStripes is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ImageStripes is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with ImageStripes.  If not, see <http://www.gnu.org/licenses/>.

"""Take one (vertical) stripes from a set of images, to create a new image.

https://framagit.org/spalax/bricabrac/-/tree/main/imagestripes
"""

import argparse
import contextlib
import os
import pathlib

from PIL import Image
from rich import progress

NAME = "ImageStripes"
VERSION = "0.2.0"


def _merge_names(names, *, extension):

    # Compute length of common prefix
    index = 0
    try:
        while len({str(name)[index] for name in names}) == 1:
            index += 1
    except IndexError:
        index = len(str(names[0]))

    if index == 0:
        return f"stripes{extension}"
    if os.path.isdir(str(names[0])[:index]):
        return os.path.join(
            f"{str(names[0])[:index]}",
            f"stripes{extension}",
        )
    return f"{str(names[0])[:index]}-stripes{extension}"


def _type_float1(text):
    """Argparse type: a float greater than 1 (or equal to 1)."""
    try:
        number = float(text)
        if number < 1:
            raise argparse.ArgumentTypeError(
                f"""Number "{text}" must be greater than 1."""
            )
        return number
    except ValueError as error:
        raise argparse.ArgumentTypeError(
            f"""Could not parse float "{text}"."""
        ) from error


def parse():
    """Parse command line arguments, and returns the (processed) options."""
    # pylint: disable=line-too-long
    parser = argparse.ArgumentParser(
        description="""Create an image composed of stripes, one for each source images.""",
        add_help=True,
    )

    parser.add_argument(
        "--version", action="version", version=f"{NAME} version {VERSION}"
    )

    parser.add_argument(
        "-f", "--force", action="store_true", help="Overwrite output file."
    )

    parser.add_argument(
        "-s",
        "--scale",
        type=_type_float1,
        default=2,
        help="Proportion of each image stripe, given that `--scale=1` means that the stripes do not overalp.",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        help="Name of output file.",
        default=None,
        type=pathlib.Path,
    )

    parser.add_argument(
        "files", nargs="+", help="Source files to process.", type=pathlib.Path
    )

    options = parser.parse_args()

    # Output file
    if options.output is None:
        options.output = pathlib.Path(
            _merge_names(
                list(name.parent / name.stem for name in options.files),
                extension=".png",
            )
        )

    # Overwrite?
    if options.output.exists() and not options.force:
        raise SystemExit(
            f"""Output file "{options.output}" already exists. Aborting."""
        )

    return options


class CachedImages(contextlib.AbstractContextManager):
    """Open images, limiting the number of files opened simultaneously.

    >>> images = CachedImages("foo.jpg", "bar.png", "baz.gif")
    >>> # Get the color of pixel at coordinates (19, 23) of image 1 (i.e. "bar.png").
    >>> images[1, 19, 23]

    This class ensures that at most ``self.size`` images are open simultaneously
    (limiting RAM occupation).
    It handles opening and closing images as necessary.
    """

    def __init__(self, names, *, cachesize=1):
        self.names = [str(name) for name in names]
        self._images = {}
        self._cachesize = cachesize
        self._lru = []
        self._size = None

    def __len__(self):
        return len(self.names)

    def __exit__(self, *args, **kwargs):
        for image in self._images.values():
            image.close()
        return super().__exit__(*args, **kwargs)

    @property
    def size(self):
        """Return the size (width, height) of the first image."""
        if self._size is None:
            self._size = self[0].size
        return self._size

    def __getitem__(self, key):

        if key not in self._images:
            # Image is not opened yet.
            if len(self._images) >= self._cachesize:
                # To many images in cache. Close the oldest one.
                self._images.pop(self._lru.pop()).close()

            self._images[key] = Image.open(self.names[key])
            self._lru.insert(0, key)

        return self._images[key]


def between(lower, x, upper):
    """Return x if lower <= x <= upper, or one of the boundary otherwise.

    >>> between(1, 3, 4)
    3
    >>> between(1, 0, 4)
    1
    >>> between(1, 8, 4)
    4
    """

    return min(max(lower, x), upper)


def images2stripes(src, dest, scale):
    """Create a new `dest` image by taking one stripe from each of the source images."""
    # pylint: disable=too-many-locals

    with (
        CachedImages(src) as sources,
        Image.new("RGB", sources.size) as destimage,
    ):
        # Check that all source images have the same size
        if len({image.size for image in sources}) != 1:
            raise SystemExit("All images must have the same size (resolution).")

        # Compute dissolve data
        width = sources.size[0]

        # dissolve is an array as wide as the image.
        # dissolve[x] contains the information about the source images that
        # will be merged to procuce de column `x` of the destination image.
        # dissolve[x] is a dictionary, where:
        # - keys are index of images
        # - values are a float.
        #
        # For instance, consider that:
        # dissolve[4] = {
        #     1: .5,
        #     3: .3,
        #     4: .2,
        # }
        # This means that column 4 of destination image will be built
        # with columns 4 of source images number 1, 3, 4,
        # with weights 0.5, 0.3, 0.2 (as in weighted arithmetic mean).
        dissolve = [{} for _ in range(width)]

        # Suppose we want to merge images 0 to 5, with scale of about 1.5.
        # The destination image is 23 columns wide, so each stripe would be
        # about 5 columns wide. But with the scale of 1.5, each scale will be
        # 5*1.5=7 columns (about), with a bit of overlap.
        #
        # abcdefghijklmnopqrstuvw
        # +-----+ +-----+ +-----+
        # |0    | |2    | |5    |
        # |   +-----+ +-----+   |
        # |   |1    | |3    |   |
        # |   |     | |     |   |
        # +---|     |-|     |---+
        #     |     | |     |
        #     +-----+ +-----+
        #
        # Columns `a` to `d` will copy image `0`.
        # Columns `e` to `g` will gradually dissolve image `0` to image `1`.
        # Column `h` will be a copy of the same column of image `1`.
        # Columns `i` to `k` will gradually dissolve image `1` into image `2`.
        # And so on.
        #
        # First, we compute the length of the one "stripe":
        # - `width` is the image width (in pixel),
        # - `scale` is the scale of the stripe, relative to the stripe width if
        #   stripes did not overlap,
        # - `len(sources)` is the totat number of source images.
        #   We get the following formula (the proof is left to the reader).
        stripewidth = (scale * width) / len(sources)

        for index in range(len(sources)):
            # Only one (vertical) stripe of each image is copied to the destination image.
            # For each of this stripe, we compute:
            # - `start`: the first column of the stripe
            # - `end`: the last column of the stripe
            # - `middle`: the center.
            middle = ((2 * index + 1) * width) / (2 * len(sources))
            start = middle - stripewidth / 2
            end = middle + stripewidth / 2

            # Then, we consider that when copied into the destination image,
            # the first and last column will have a weight 0,
            # and the middle one will have a weight of 1
            # (that way, the center of the stripe should be mainly *this* image,
            # and the image will gradually fade out as we go further left or right)
            #
            # The following loop ensures that, for each column x of the stripe:
            # - if `x == start` (not exactly, see below), image weight is 0
            # - if `x == end`, image weight is 0
            # - if `x == middle`, image weight is 1.
            # Actually, image weight is 0 on `x == start - 1` instead of `x == start`,
            # so that the leftmost column of the destination image is not empty.
            #
            # Remember that `dissolve[14][8] = .3` means:
            # "The 14th column of the 8th image will appear in the 14th column
            # of the destination image with a weight of .3 (which can be a lot,
            # or not, depending of the weights of other images which appear on
            # the same column)".
            for x in range(max(0, round(start)), min(width, round(end))):
                if x <= middle:
                    dissolve[x][index] = between(
                        0, 2 * (x - start + 1) / stripewidth, 1
                    )
                else:
                    dissolve[x][index] = between(0, 2 * (end - x) / stripewidth, 1)

        # At last, we compute the total weight of each column,
        # which will be used to compute the weighted mean.
        for blend in dissolve:
            blend["total"] = sum(blend.values())

        # Create dest image
        # We iterate image per image to prevent opening and closing images a lot
        for sourceindex in range(len(sources)):
            for x, blend in progress.track(
                tuple(
                    # We thin out columns in which image `sourceindex` does not appear
                    (x, blend)
                    for x, blend in enumerate(dissolve)
                    if sourceindex in blend
                ),
                description=f"Image {sourceindex + 1}/{len(sources)}",
                transient=True,
            ):
                # Image is to be blended in this vertical stripe. Add it.
                for y in range(sources.size[1]):
                    destimage.putpixel(
                        (x, y),
                        tuple(
                            # We keep the color that was already present
                            destimage.getpixel((x, y))[key]
                            # We add the color of image `sourceindex`, with the right weight
                            + round(color * blend[sourceindex] / blend["total"])
                            for key, color in enumerate(
                                sources[sourceindex].getpixel((x, y))
                            )
                        ),
                    )

        # Save dest image
        destimage.save(dest)


def main():
    """Main function."""
    options = parse()
    images2stripes(options.files, options.output, options.scale)


if __name__ == "__main__":
    main()
