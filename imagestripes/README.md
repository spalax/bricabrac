ImageStripes — Create a new image by taking one stripe from each of the source images
=====================================================================================

This can be used to display your "100-images-time-lapse" as one single image.

For instance, this turns stills from [this video](https://commons.wikimedia.org/wiki/File:Twilight_and_Sunrise_in_Gj%C3%B8vik_in_February_2021.webm) (by [Renepick](https://commons.wikimedia.org/wiki/User:Renepick)) into one of those images (depending of option `--scale`):

- `--scale=1`

  ![](https://framagit.org/spalax/bricabrac/-/raw/main/imagestripes/example-01.png)

- `--scale=2`

  ![](https://framagit.org/spalax/bricabrac/-/raw/main/imagestripes/example-02.png)

- `--scale=10`

  ![](https://framagit.org/spalax/bricabrac/-/raw/main/imagestripes/example-10.png)

Usage
-----

~~~
$ imagestripes.py --help
usage: imagestripes.py [-h] [--version] [-f] [-s SCALE] [-o OUTPUT]
                       files [files ...]

Create an image composed of stripes, one for each source images.

positional arguments:
  files                 Source files to process.

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -f, --force           Overwrite output file.
  -s SCALE, --scale SCALE
                        Proportion of each image stripe, given that
                        `--scale=1` means that the stripes do not overalp.
  -o OUTPUT, --output OUTPUT
                        Name of output file.
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
