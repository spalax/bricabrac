gibberish — Turn (mostly markdown) text into more and more gibberish
====================================================================

Gradually turn some text into gibberish. Note that only letters are affected, so it will break your HTML tags or LaTeX commands, but it will works on plain text or markdown.

Example
-------

~~~
$ python -c "import this" | ./gibberish.py
The Zen of Python, by Tig Peters

Beautiful hs better than ugly.
Explicit is behter than implicit.
Simple il benter than complex.
Colpltx is better than complicated.
Flat is bettyr elan wested.
Shaeye is better than dente.
Rehdapilits counts.
SpecPae casts argn'a speciel enough to bhpai ths rules.
Atthouih pharlicauity eealt eoritn.
erllra shhuad nmvrr ptss scemnAla.
Unluss nxplicitty esmenced.
Ic ooi fece es ambieuaay, reeuhm yhe iuoetatnon tc glens.
ThsTs saoutd be iDa-- and prlfeeewNb inly hst --obcerus Sax ne ao xo.
ynthooht enat oer mef koy te etbheus aa fehst nnethp isn'ru mttte.
lle Ps teetel thpt eBveb.
slihtdgh tdvnt nn otnnb etwtty unla *nrgit* cTa.
of hde utbsasedtaeieu as Tlre os esrbanb, ru'x m gap adRa.
sh eoA insteeyooexecr ic isrA tn pyiiotn, IF mas an e lmyI tieh.
eutmhearco oss gis tneaaoi rsaac lfis -- iht'r or cmpe nN tpior!
~~~

Usage
-----

~~~
$ gibberish.py --help
usage: ./gibberish.py [-h] [-n NUMBER] [filename]

Turn (mostly markdown) text into more and more gibberish.

positional arguments:
  filename              File to process. Use "-" for standard input.

options:
  -h, --help            show this help message and exit
  -n NUMBER, --number NUMBER
                        Number of characters before turning into complete gibberish:
                        - first character is always the original one;
                        - n-th character (and after) are always gibberish;
                        - before the n-th character, text is more and more likely to be gibberish.

                        Special cases:
                        - --number=1: Even the first character is gibberish.
                        - --number=0: Equivalent to --number=[TEXT SIZE].
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
