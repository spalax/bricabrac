#!/usr/bin/env python

# Copyright 2023 Louis Paternault
#
# Gibberish is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Gibberish is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Gibberish.  If not, see <http://www.gnu.org/licenses/>.

"""Turn (mostly markdown) text into more and more gibberish."""

import argparse
import random
import re
import sys
import textwrap

WORDCHARS = re.compile(r"\w")


def parse():
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        prog=sys.argv[0],
        description="Turn (mostly markdown) text into more and more gibberish.",
        formatter_class=argparse.RawTextHelpFormatter,
    )
    parser.add_argument(
        "filename",
        nargs="?",
        default="-",
        type=argparse.FileType("r", encoding="utf8"),
        help=textwrap.dedent(
            """\
            File to process. Use "-" for standard input.
            """
        ),
    )
    parser.add_argument(
        "-n",
        "--number",
        default=0,
        type=int,
        help=textwrap.dedent(
            """\
            Number of characters before turning into complete gibberish:
            - first character is always the original one;
            - n-th character (and after) are always gibberish;
            - before the n-th character, text is more and more likely to be gibberish.

            Special cases:
            - --number=1: Even the first character is gibberish.
            - --number=0: Equivalent to --number=[TEXT SIZE].
    """
        ),
    )
    return parser.parse_args()


def main():
    """Main function."""

    # Parse options
    options = parse()
    text = options.filename.read()
    if options.number == 0:
        options.number = len(text)

    # Parse source text
    chars = [char for char in text if WORDCHARS.match(char)]

    # Generate output
    for i, source in enumerate(text):
        if (
            WORDCHARS.match(source)  # Character is a letter
            and random.randrange(options.number) <= i  # Random!
        ):
            print(random.choice(chars), end="")
        else:
            print(source, end="")


if __name__ == "__main__":
    main()
