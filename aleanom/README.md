aleanom — Select a random name (consistent with names given that year)
======================================================================

Select a random name from a given year, from a given country.

Usage
-----

~~~
$ aleanom.py --help
usage: __main__.py [-h] [--version] [-y YEARS] [-g GENDERS] [-n NUMBER]
                   {france}

Select a random name (consistent with names given that year)

positional arguments:
  {france}              Name of the database. Available databases are: france.

options:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -y YEARS, --years YEARS
                        Year (e.g. 1986) or year range (e.g. 1978-1986)
  -g GENDERS, --genders GENDERS
                        Genders of the name, as a comma-separated list among:
                        man, woman. Also accepts "any". What about other
                        genders? As I am writing this, the only database used
                        is the official French one, which only recognize man
                        and woman.
  -n NUMBER, --number NUMBER
                        Number of names to print.
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
