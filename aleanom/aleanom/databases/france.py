# Copyright 2023 Louis Paternault
#
# This file is part of AleaNom.
#
# AleaNom is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AleaNom is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with AleaNom.  If not, see <http://www.gnu.org/licenses/>.

import collections
import csv
import enum
import io
import pathlib
import typing
import zipfile

import requests
import xdg

from .. import Gender

# From https://www.insee.fr/fr/statistiques/2540004
URL = "https://www.insee.fr/fr/statistiques/fichier/2540004/nat2021_csv.zip"
CACHEDIR = pathlib.Path(xdg.xdg_cache_home()) / "aleanom"
CACHEFILE = CACHEDIR / "database-france.csv"


def get_data():
    if not CACHEFILE.exists():
        CACHEDIR.mkdir(exist_ok=True)

        # Download zip file
        response = requests.get(URL, verify=True, stream=True)
        response.raise_for_status()
        response.raw.decode_content = True

        # Uncompress archive
        with (
            zipfile.ZipFile(io.BytesIO(response.content), mode="r") as archive,
            open(CACHEFILE, mode="wb") as file,
        ):
            file.write(archive.read(archive.namelist()[0]))

    with open(CACHEFILE, newline="") as csvfile:
        yield from csv.DictReader(csvfile, delimiter=";")


def filter(years: typing.Container, genders: enum.Flag) -> collections.Counter:
    counter = collections.defaultdict(int)
    for row in get_data():
        try:
            if (
                # Check year
                int(row["annais"]) in years
                # Check name
                and not row["preusuel"].startswith("_")
                # Check gender
                and {"1": Gender.MAN, "2": Gender.WOMAN}[row["sexe"]] in genders
            ):
                counter[row["preusuel"]] += int(row["nombre"])
        except ValueError as error:
            continue

    return collections.Counter(counter)
