# Copyright 2023 Louis Paternault
#
# This file is part of AleaNom.
#
# AleaNom is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AleaNom is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with AleaNom.  If not, see <http://www.gnu.org/licenses/>.

"""Select a random name (consistent with names given that year)"""

import collections
import dataclasses
import enum
import importlib
import math
import numbers
import random
import typing


class Gender(enum.Flag):
    """Gender: man or woman.

    Other genders might be added later.
    """

    MAN = enum.auto()
    WOMAN = enum.auto()


ANYGENDER = Gender.MAN | Gender.WOMAN

DATABASES = ["france"]


class AleaNomError(Exception):
    """Generic exception raised by this module."""


@dataclasses.dataclass
class Interval(collections.abc.Container):
    """An interval between two numbers."""

    lower: numbers.Number | None = None
    upper: numbers.Number | None = None

    def __post_init__(self):
        if self.upper is None and self.lower is None:
            self.lower = -math.inf
            self.upper = math.inf
        elif self.upper is None:
            self.upper = self.lower
        elif self.lower is None:
            self.lower = self.upper

    def __contains__(self, key):
        return self.lower <= key <= self.upper


def aleanom(
    database: str, *, years: typing.Container, genders: enum.Flag, number: int
) -> typing.Iterable[str]:
    """Iterate over random names matching the constraints."""
    weightednames = (
        importlib.import_module(f".{database}", "aleanom.databases")
        .filter(years=years, genders=genders)
        .items()
    )
    if not weightednames:
        raise AleaNomError("Not a single name matches those constraints…")
    names, weights = list(zip(*weightednames))
    for name in random.choices(names, weights=weights, k=number):
        yield name.title()
