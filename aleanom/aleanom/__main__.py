#!/usr/bin/env python3

# Copyright 2023 Louis Paternault
#
# This file is part of AleaNom.
#
# AleaNom is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# AleaNom is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with AleaNom.  If not, see <http://www.gnu.org/licenses/>.

"""aleanom — Select a random name (consistent with names given that year)

https://framagit.org/spalax/bricabrac/-/tree/main/aleanom
"""

import argparse
import functools
import logging
import operator
import re
import sys
import textwrap

from . import ANYGENDER, DATABASES, AleaNomError, Gender, Interval, aleanom

NAME = "aelanom"
VERSION = "0.1.0"


RE_YEAR = re.compile(r"^(\d{4})$")
RE_YEARS = re.compile(r"^(\d{4})-(\d{4})$")


def _type_years(text):
    if match := RE_YEAR.match(text):
        return Interval(
            int(match.groups()[0]),
        )
    if match := RE_YEARS.match(text):
        return Interval(*(int(boundary) for boundary in match.groups()))

    raise argparse.ArgumentTypeError(f"{text} is not a year or year range.")


def _type_genders(text):
    if text == "any":
        return ANYGENDER

    try:
        return functools.reduce(
            operator.or_,
            (getattr(Gender, gender.upper()) for gender in text.split(",")),
        )
    except AttributeError as error:
        raise argparse.ArgumentTypeError(
            f"'{error.name}' is an unknown gender."
        ) from error


def _type_number(text):
    try:
        number = int(text)
    except ValueError as error:
        raise argparse.ArgumentTypeError(
            "Argument to --number must be positive integer."
        ) from error
    if number < 0:
        raise argparse.ArgumentTypeError("Number must be positive.")
    return number


def parse():
    """Parse command line arguments, and returns the path."""
    parser = argparse.ArgumentParser(
        description="""Select a random name (consistent with names given that year)""",
        add_help=True,
    )
    parser.add_argument(
        "--version", action="version", version=f"{NAME} version {VERSION}"
    )
    parser.add_argument(
        "-y",
        "--years",
        default=Interval(),
        help="Year (e.g. 1986) or year range (e.g. 1978-1986)",
        type=_type_years,
    )
    parser.add_argument(
        # pylint: disable=line-too-long
        "-g",
        "--genders",
        default=ANYGENDER,
        type=_type_genders,
        help=textwrap.dedent(
            """\
                    Genders of the name, as a comma-separated list among: {}. Also accepts "any".
                    What about other genders? As I am writing this, the only database used is the official French one, which only recognize man and woman.
                                 """
        ).format(", ".join(sorted(item.name.lower() for item in Gender))),
    )

    parser.add_argument(
        "-n",
        "--number",
        type=_type_number,
        help="Number of names to print.",
        default=1,
    )

    parser.add_argument(
        "database",
        choices=DATABASES,
        help="Name of the database. Available databases are: {}.".format(  # pylint: disable=consider-using-f-string
            ", ".join(DATABASES)
        ),
    )

    return parser.parse_args()


def main():
    """Main function."""
    args = parse()
    try:
        for name in aleanom(
            database=args.database,
            years=args.years,
            genders=args.genders,
            number=args.number,
        ):
            print(name)
    except AleaNomError as error:
        logging.error(error)
        sys.exit(1)


if __name__ == "__main__":
    main()
