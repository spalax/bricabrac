#!/bin/bash

# Copyright 2015-2021 Louis Paternault
#
# CheckCopyrightYear is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# CheckCopyrightYear is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with CheckCopyrightYear.  If not, see <http://www.gnu.org/licenses/>.

# Display name of files where year of last commit does not appear in the file (as "Copyright XXXX John Doe")
#
# USAGE
#
# checkcopyrightyear PATTERN
#
# EXAMPLE
#
# checkcopyrightyear "*.py"
#

set -e

pattern="$1"

function foo() {
  (
    file="$1"
    cd "$(dirname "$file")"
    file="$(basename "$file")"
    if [ -z "$(git ls-files -- "$file")" ]
    then
      :
    else
      year=$(git log --date-order --pretty="%ai" --max-count=1 -- "$file" | sed 's/-.*//')
      if [ -z "$year" ]
      then
        return
      fi
      grep $year "$file" -q || echo $1 $year
    fi
  )
}

export -f foo
find . -not -path '*/\.*' -name "$pattern" -exec bash -c 'foo "$0"' {} \;
