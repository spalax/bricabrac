checkcopyrightyear — Display name of files containing errors in copyright year
==============================================================================

Given a file pattern, for every file (handled by git) matching this pattern, check that the year of the last commit (of this file) appears in the file (hopefully as a "Copyright XXXX John Doe").

Usage
-----

~~~sh
$ ./checkcopyrightyear.sh PATTERN

# Example:

$ ./checkcopyrightyear.sh "*.py"
~~~

Install
-------

0. Download or clone this repository, and move to the right directory.
0. Copy the ``checkcopyrightyear.sh`` into a directory of your ``PATH`` environment variable.
