#!/usr/bin/env python3

# Copyright 2023 Louis Paternault
#
# pdf2webp is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# pdf2webp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with pdf2webp.  If not, see <http://www.gnu.org/licenses/>.

"""Convert a PDF to a WEBP animated image (each PDF page is a WEBP frame)."""

import pathlib
import subprocess
import sys
import tempfile

from PIL import Image


def main():
    """Main function."""
    sourcefile = pathlib.Path(sys.argv[1])
    with tempfile.TemporaryDirectory() as tempdir:
        # PDF to PNG
        subprocess.check_call(
            [
                "pdftocairo",
                "-png",
                "-r",
                "600",
                "-transp",
                sourcefile,
                pathlib.Path(tempdir) / "frame",
            ]
        )

        # PNG to WEBP
        frames = [
            Image.open(frame)
            for frame in sorted(pathlib.Path(tempdir).glob("frame*.png"))
        ]
        frames[0].save(
            sourcefile.with_suffix(".webp"),
            append_images=frames[1:],
            duration=1000,
            save_all=True,
        )


if __name__ == "__main__":
    main()
