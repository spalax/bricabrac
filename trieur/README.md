trieur — Sort media according to their creation date (found in metadata)
========================================================================

Trieur takes a single argument: a directory where media files are located. Their metadata is read, and the files are renamed as ``../YYYY/MM/YYYYMMDD-hhmmss.ext``. Media with the exact same creation date are correctly handled (a suffix is appended). Thus, the expected structure of your media directory is:

~~~
+ 2020
  + 01
    20210112-121212.png
  + 12
    20211224-110003.mp4
+ 2021
  + 04
    + 20210412-040000.jpg
+ sort
~~~

With that structure, you can run:

~~~sh
$ trieur.py sort/
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
