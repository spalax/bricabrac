#!/usr/bin/env python3

# Copyright 2015-2021 Louis Paternault
#
# Trieur is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Trieur is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero Public License for more details.
#
# You should have received a copy of the GNU Affero Public License
# along with Trieur.  If not, see <http://www.gnu.org/licenses/>.

"""Media sorter

Sort media according to their creation date (found in metadata).
"""

import collections
import contextlib
import datetime
import glob
import hashlib
import itertools
import logging
import mimetypes
import os
import re
import shutil
import sys

try:
    import exifread
    from hachoir.parser import createParser
    from hachoir.metadata import extractMetadata
except ImportError:
    # pylint: disable=line-too-long
    print(
        "Erreur: Des paquets nécessaires ne sont pas installés. Exécuter la commande suivante pour les installer."
    )
    print("sudo apt-get install python3-exifread hachoir")
    sys.exit(1)

logging.basicConfig()
LOGGER = logging.getLogger()
LOGGER.setLevel(logging.INFO)

PHOTODIR = sys.argv[1]
TRIEURDIR = os.path.join(PHOTODIR, "trier")

DEST_TEMPLATE = (
    PHOTODIR,
    "{year:04d}",
    "{month:02d}",
    "{year:04d}{month:02d}{day:02d}-{time}",
)

RE_DATETIME = re.compile(
    r"""
    (?P<year>\d*):(?P<month>\d*):(?P<day>\d*)  # Year
    \ *                                        # Space
    (?P<hour>\d*):(?P<minute>\d*):(?P<second>\d*) # Time
    """,
    re.VERBOSE,
)

################################################################################
# Utils


@contextlib.contextmanager
def nostderr():
    """Disable output on standard error."""
    with open(os.devnull, "w") as null:
        save_std = sys.stderr
        sys.stderr = null
        yield
        sys.stderr = save_std


def md5sum(filename):
    """Return the md5sum of the file given in argument."""
    with open(filename, "rb") as filecontent:
        md5 = hashlib.md5()
        md5.update(filecontent.read())
        return md5.digest()


class FileAlreadyExists(Exception):
    """A file is already present in the destination directory."""

    def __init__(self, source, dest):
        super().__init__()
        self.source = source
        self.dest = dest

    def __str__(self):
        return "Le fichier '{}' existe déjà : '{}'.".format(self.source, self.dest)


class NoCreationDate(Exception):
    """Creation date could not be found in the metadata."""

    def __init__(self, image):
        super().__init__()
        self.image = image

    def __str__(self):
        return "Le fichier '{}' n'a pas de date de prise de vue.".format(self.image)


def get_datetime(path):
    """Renvoit la date de prise de vue du média donné en argument."""
    if mimetypes.guess_type(path)[0] == "image/jpeg":
        with open(path, "rb") as jpegfile:
            try:
                return datetime.datetime(
                    **{
                        key: int(value)
                        for key, value in RE_DATETIME.match(
                            exifread.process_file(jpegfile)[
                                "EXIF DateTimeOriginal"
                            ].values
                        )
                        .groupdict()
                        .items()
                    }
                )
            except KeyError:
                pass
    with nostderr():
        try:
            return extractMetadata(createParser(path)).get("creation_date")
        except (ValueError, AttributeError) as error:
            raise NoCreationDate(path) from error


################################################################################
# Move medias


class TimedMedia:
    """A media (photo, video), with a creation date."""

    def __init__(self, path):
        self.path = path
        self.datetime = get_datetime(path)

    @property
    def extension(self):
        """Return the file extension (without the dot)."""
        return self.path.split(".")[-1]

    @classmethod
    def is_media(cls, path):
        """Return `True` iff the argument is a media (photo or video)"""
        filetype = mimetypes.guess_type(path)[0]
        if not isinstance(filetype, str):
            return False
        return filetype.split("/")[0] in ["image", "video"]

    def destname(self):
        """Compute and return the destination name"""
        for name in self.iter_names():
            if not os.path.exists(name):
                return name
        raise Exception("Erreur interne…")

    def formatter(self):
        """Return the dictionnary to feed the string formatter."""
        return {
            "year": self.datetime.year,
            "month": self.datetime.month,
            "day": self.datetime.day,
            "time": self.datetime.time().strftime("%H%M%S"),
        }

    def iter_names(self):
        """Iterate over possible destination names for this file."""
        base = os.path.join(
            *[part.format(**self.formatter()) for part in DEST_TEMPLATE]
        )
        yield base + "." + self.extension
        for count in itertools.count():
            yield base + "-" + str(count) + "." + self.extension

    def move(self, destname):
        """Move the file to the destination."""

        # Check if file already exist
        base = os.path.join(
            *[part.format(**self.formatter()) for part in DEST_TEMPLATE]
        )
        for image in glob.glob(base + "*"):
            if os.stat(image).st_size == os.stat(self.path).st_size:
                if md5sum(image) == md5sum(self.path):
                    raise FileAlreadyExists(self.path, image)

        # Create directory
        try:
            os.makedirs(os.path.dirname(destname))
        except OSError as error:
            if error.errno != 17:
                raise

        # Actual move
        shutil.move(self.path, destname)


class MediaSorter:
    """Sort media file in organized files and directories."""

    # pylint: disable=too-few-public-methods

    def __init__(self, directory):
        self.medias = collections.defaultdict(list)
        self.directory = directory

        LOGGER.info("Analyse des médias")
        for image in sorted(self._iter_medias(self.directory)):
            LOGGER.info(image)
            if not TimedMedia.is_media(image):
                continue
            try:
                timed = TimedMedia(image)
            except NoCreationDate as error:
                LOGGER.error(error)
                continue
            self.medias[timed.datetime].append(timed)

    def move(self):
        """Move media"""
        LOGGER.info("Déplacement des médias")
        for time in sorted(self.medias):
            for image in self.medias[time]:
                try:
                    destname = image.destname()
                    LOGGER.info("Déplacement de '%s' vers '%s'.", image.path, destname)
                    image.move(destname)
                except FileAlreadyExists as error:
                    LOGGER.error(error)
                    continue

    @staticmethod
    def _iter_medias(directory):
        """Iterate over media names to move"""
        for root, _, files in os.walk(directory):
            for filename in files:
                yield os.path.join(root, filename)


################################################################################
# Main

if __name__ == "__main__":
    MediaSorter(TRIEURDIR).move()
