#!/usr/bin/env python3

# Copyright 2021 Louis Paternault
#
# PdfCompress is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# PdfCompress is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with PdfCompress.  If not, see <http://www.gnu.org/licenses/>.

"""Compress a PDF file.

This is a wrapper to ghostscript.

https://framagit.org/spalax/bricabrac/-/tree/main/pdfcompress
"""

import argparse
import subprocess
import pathlib

NAME = "pdfcompress"
VERSION = "0.1.0"

COMPRESSION_LEVELS = {
    1: "screen",
    2: "ebook",
    3: "prepress",
    4: "print",
}


def _type_level(text):
    try:
        level = int(text)
        if level not in COMPRESSION_LEVELS:
            raise ValueError()
        return level
    except ValueError as error:
        raise argparse.ArgumentTypeError(
            "Level must be an integer between 1 and 4."
        ) from error


def parse():
    """Parse command line arguments, and returns the (processed) options."""
    parser = argparse.ArgumentParser(
        description="""Compress a PDF file.""",
        add_help=True,
    )

    parser.add_argument(
        "--version", action="version", version=f"{NAME} version {VERSION}"
    )

    parser.add_argument(
        "FILE",
        help="File to compress.",
        type=pathlib.Path,
        default="-",
        nargs="?",
    )

    parser.add_argument(
        "-o",
        "--output",
        action="store",
        help="Name of output file.",
        default=None,
    )

    parser.add_argument(
        # pylint: disable=line-too-long
        "-l",
        "--level",
        type=_type_level,
        default=2,
        help="Compression level (integer between 1 and 4), smaller number means less compression (and bigger file).",
    )

    options = parser.parse_args()
    if options.output is None:
        if str(options.FILE) == "-":
            options.output = "-"
        else:
            # pylint: disable=line-too-long
            options.output = f"{options.FILE.parent / options.FILE.stem}-compress{options.FILE.suffix}"

    return options


def main():
    """Main function."""
    options = parse()
    subprocess.run(
        (
            "ghostscript",
            "-sDEVICE=pdfwrite",
            "-dCompatibilityLevel=1.4",
            f"-dPDFSETTINGS=/{COMPRESSION_LEVELS[options.level]}",
            "-dNOPAUSE",
            "-dQUIET",
            "-dBATCH",
            f"-sOutputFile={options.output}",
            options.FILE,
        ),
        check=True,
    )


if __name__ == "__main__":
    main()
