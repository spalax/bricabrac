pdfcompress — Compress a PDF file
=================================

This is a wrapper to ``ghostscript``.

Usage
-----

~~~
$ ./pdfcompress.py --help
usage: pdfcompress.py [-h] [--version] [-o OUTPUT] [-l LEVEL] [FILE]

Compress a PDF file.

positional arguments:
  FILE                  File to compress.

optional arguments:
  -h, --help            show this help message and exit
  --version             show program's version number and exit
  -o OUTPUT, --output OUTPUT
                        Name of output file.
  -l LEVEL, --level LEVEL
                        Compression level (integer between 1 and 4), smaller number means less compression (and bigger file).
"""
~~~

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). If you think it is mature/interesting enough to be published there, [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to publish it on PyPI.

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
