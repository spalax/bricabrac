Bric-à-brac 👝 A rag bag of (mostly) python tools
=================================================

Those tools are not mature or interesting enough to deserve their own repository yet. That's why you will not find them on [PyPI](https://pypi.org), [ReadTheDocs](https://rtfd.io). I might make backward incompatible changes; they might become unmaintained without notice.

Nonetheless, if you think one of them deserves some love (and a proper PyPI package or documentation), [open an issue](https://framagit.org/spalax/bricabrac/-/issues): this might motivate me to spend some work on it.

List of tools:

- [aleanom](https://framagit.org/spalax/bricabrac/-/tree/main/aleanom): Select a random name, consistent with names given that year.
- [argecir](https://framagit.org/spalax/bricabrac/-/tree/main/argecir): Generate circular genealogy trees.
- [benchmark](https://framagit.org/spalax/bricabrac/-/tree/main/benchmark): Run several commands several times, and save their execution time
- [changewallpaper](https://framagit.org/spalax/bricabrac/-/tree/main/changewallpaper): Choose a random image from a given repository, and set it as the desktop wallpaper (Gnome3 only).
- [checkcopyrightyear/](https://framagit.org/spalax/bricabrac/-/tree/main/checkcopyrightyear): Display name of files where year of last commit does not appear in the file (as "Copyright XXXX John Doe")
- [debut](https://framagit.org/spalax/bricabrac/-/tree/main/debut): Open a window to choose programs to launch at startup.
- [describeimages](https://framagit.org/spalax/bricabrac/-/tree/main/describeimage): A tiny GUI tool to add a description to images (jpg, png, etc.).
- [gibberish](https://framagit.org/spalax/bricabrac/-/tree/main/gibberish): Turn (mostly markdown) text into more and more gibberish.
- [id3fixer](https://framagit.org/spalax/bricabrac/-/tree/main/id3fixer): Fix some errors in id3 tags of mp3 files.
- [imagestripes](https://framagit.org/spalax/bricabrac/-/tree/main/imagestripes): Create a new image by taking one stripe from each of the source images
- [pdf2webp](https://framagit.org/spalax/bricabrac/-/tree/main/pdf2webp): Convert a PDF to a WEBP animated image (each PDF page is a WEBP frame).
- [pdfcompress](https://framagit.org/spalax/bricabrac/-/tree/main/pdfcompress): Compress a PDF file.
- [randomfile](https://framagit.org/spalax/bricabrac/-/tree/main/randomfile): Display the name of a random file.
- [trieur](https://framagit.org/spalax/bricabrac/-/tree/main/trieur): Rename media files as ``YYYY/MM/YYYYMMDD-hhmmss.ext`` files.
