#!/usr/bin/env python3

import sys

import music_tag

def show_attr(musicfile):
    for attr in dir(musicfile):
        if attr.startswith("_"):
            continue
        print(attr, getattr(musicfile, attr))
    import pprint; pprint.pprint(musicfile.mfile)

def fix_date_format(musicfile):
    try:
        musicfile['year']
        return False
    except ValueError:
        del musicfile['year']
        print("Correction de la date…")
        return True

def fix_data_error(musicfile):
    if 'tracknumber' in musicfile:
        if len(str(musicfile['tracknumber'])) > 4:
            print("Correction du numéro de piste…")
            del musicfile['tracknumber']
            return True
    return False

def fix_missing_title(musicfile):
    if 'title' not in musicfile:
        print("Correction du titre…")
        musicfile['title'] = input("Titre ? ")
        return True
    return False


def fix(filename):
    musicfile = music_tag.load_file(filename)
    show_attr(musicfile)
    if any((
        fix_date_format(musicfile),
        fix_data_error(musicfile),
        fix_missing_title(musicfile),
        )):
        musicfile.save()

if __name__ == "__main__":
    for filename in sys.argv[1:]:
        print(f"Analyse de {filename}…")
        fix(filename)
