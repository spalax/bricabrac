# Copyright Louis Paternault 2023
#
# This file is part of Argecir.
#
# Argecir is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Argecir is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Argecir. If not, see <https://www.gnu.org/licenses/>.

"""Provide tools to generate a genealogy tree (in LaTeX, or Graphviz, or…)."""

import collections
import dataclasses
import itertools
import math
import numbers
import statistics

import jinja2

from .. import gedcom


@dataclasses.dataclass
class Counter:
    """A counter. When called, it returns actual value, *then* increases it.

    >>> c = Counter()
    >>> c()
    0
    >>> c()
    1
    >>> c(2)
    2
    >>> c()
    4
    """

    _counter: int = dataclasses.field(init=False, default=0)

    def __call__(self, step=1):
        self._counter += step
        # Return previous value
        return self._counter - step


class ProportionalList(collections.UserList):
    """Proportional list

    If a float is given as a key, return the corresponding value between
    nearest integer values.

    For instance:
    - `L[2.5]` returns the mean of `L[2]` and `L[3]`.
    - `L[2.3]` returns L[2] + .3 * (L[3]-L[2])
    """

    def __getitem__(self, key):
        lower = math.floor(key)
        upper = math.ceil(key)
        if lower == upper:
            return self.data[key]
        return (
            (self.data[lower] - self.data[upper]) * key
            + lower * self.data[upper]
            - upper * self.data[lower]
        ) / (lower - upper)


def render(tree: gedcom.Family, conf: dict) -> str:
    """Render a family tree using the given template.

    Default output format is LaTeX ; graphviz is alsa available.
    """
    # pylint: disable=too-many-locals, too-many-branches
    # Compute angles
    tree.enforce_height(tree.height)

    # First, angles of leaves
    count = Counter()
    for family in tree.iter_families(height=1):
        if all(
            isinstance(child, gedcom.Phantom) for child in family.stepchildren()
        ) or all(
            (not isinstance(child, gedcom.Phantom)) for child in family.stepchildren()
        ):
            for child in sorted(family.stepchildren()):
                child.angle = count()
        else:
            realstepchildren = list(
                sorted(child for child in family.stepchildren(phantoms=False))
            )
            left = (
                count(len(list(family.stepchildren())))
                + (len(list(family.stepchildren())) - 1) / 2
                - (len(realstepchildren) - 1) / 2
            )
            for i, child in enumerate(realstepchildren):
                child.angle = left + i
            for child in family.stepchildren():
                if isinstance(child, gedcom.Phantom):
                    child.angle = left + (len(realstepchildren) - 1) / 2

    step = 360 / count()
    for individual in tree.iter_individuals(height=0):
        individual.angle = individual.angle * step

    # Set configuration
    if conf["layout"]["radius"] is None:
        conf["layout"]["radius"] = 105
    if isinstance(conf["layout"]["radius"], numbers.Number):
        conf["layout"]["radius"] = [
            conf["layout"]["radius"] * i for i in range(1, tree.height + 2)
        ]
    conf["layout"]["radius"] = ProportionalList(conf["layout"]["radius"])
    if conf["layout"].get("angles", None) is None:
        conf["layout"]["angles"] = step
    if isinstance(conf["layout"]["angles"], numbers.Number):
        conf["layout"]["angles"] = [
            conf["layout"]["angles"] * max(0, (i - 1))
            for i in range(tree.height + 1, 0, -1)
        ]
    angles = conf["layout"]["angles"]

    # Then, angles of other nodes
    for height in range(1, tree.height):
        for individual in tree.iter_individuals(height=height, partners=False):
            for family in individual.families_down:
                if len(individual.families_down) == 1:
                    partners = family.partners
                else:
                    partners = family.partners[1:]
                try:
                    center = statistics.mean(
                        child.angle for child in family.stepchildren(phantoms=False)
                    )
                except statistics.StatisticsError:
                    center = statistics.mean(
                        child.angle for child in family.stepchildren(phantoms=True)
                    )
                left = (
                    center
                    - angles[tree.height - family.height] * (len(partners) - 1) / 2
                )
                for i, partner in enumerate(reversed(partners)):
                    partner.angle = left + i * angles[tree.height - family.height]
            if len(individual.families_down) > 1:
                individual.angle = statistics.mean(
                    partner.angle for partner in individual.partners
                )
                for partner in individual.partners:
                    if (
                        abs(partner.angle - individual.angle)
                        < angles[tree.height - family.height]
                    ):
                        partner.angle = individual.angle + math.copysign(
                            angles[tree.height - family.height],
                            partner.angle - individual.angle,
                        )

    # At last, we can draw the tree
    env = jinja2.Environment(
        loader=jinja2.PackageLoader("argecir.template", "data"),
        extensions=["jinja2.ext.loopcontrols"],
        trim_blocks=True,
        lstrip_blocks=True,
    )
    return env.get_template(conf["template"]).render(tree=tree, conf=conf)
