# Copyright Louis Paternault 2023
#
# This file is part of Argecir.
#
# Argecir is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Argecir is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Argecir. If not, see <https://www.gnu.org/licenses/>.

"""Command line parser."""

import argparse
import fileinput
import pathlib
import tomllib

from . import gedcom, template

DEFAULT_CONF = {
    "file": "-",
    "template": "argecir.tex",
    "layout": {
        "unit": "1mm",
        "leaf": {
            "width": 28,
            "height": 15,
        },
        "radius": None,
        "angles": None,
    },
}


def _fill_in_the_blanks(data, default):
    for key in default:
        if key in data:
            if isinstance(data[key], dict):
                _fill_in_the_blanks(data[key], default[key])
        else:
            data[key] = default[key]


def _load_configuration(filename, *, must_exist=True):
    try:
        with open(filename, mode="r", encoding="utf8") as conffile:
            return tomllib.loads("".join(conffile.readlines()))
    except FileNotFoundError:
        if must_exist:
            raise
        return {}


def parse_cli() -> dict:
    """Parse command line arguments, and return configuration.

    Configuration is a dictionary having the schema of `DEFAULT_CONF`.
    """
    parser = argparse.ArgumentParser(
        # pylint: disable=line-too-long
        prog="argecir",
        description="Generate a circular genealogy tree.",
        epilog="Input: A GEDCOM database or TOML configuration file. Output: LaTeX code, which has to be compiled by LuaLaTeX to produce a PDF file.",
    )
    parser.add_argument("file", help="GED or TOML file", type=pathlib.Path)

    args = parser.parse_args()

    # Find name of configuration file
    if args.file.suffix == ".ged":
        # Configuration file associated to foo.ged is foo.toml
        conf = _load_configuration(
            args.file.with_suffix(".toml"),
            must_exist=False,
        )
        conf["file"] = args.file
    elif args.file.suffix == ".toml":
        conf = _load_configuration(args.file)
        if "file" in conf:
            conf["file"] = args.file.parent / conf["file"]
        else:
            conf["file"] = args.file.with_suffix(".ged")
    else:
        raise argparse.ArgumentTypeError(
            f"Argument should be a `toml` or `ged` file (not a `{args.file.suffix}` file)."
        )

    _fill_in_the_blanks(conf, DEFAULT_CONF)

    return conf


def parse_gedcom(conf) -> gedcom.Family:
    """Parse a genealogy tree, and produce a `gedcom.Family` object.

    Tree is stored as a file in `conf["file"]`.

    See variable `DEFAULT_CONF` for more information about this configuration dictionary.
    """
    with fileinput.input(conf["file"], encoding="utf8") as file:
        return gedcom.parse(
            (line.strip() for line in file),
            center=conf.get("center", None),
        )


def main():
    """Main function."""
    # Parse command line arguments
    conf = parse_cli()
    print(template.render(parse_gedcom(conf), conf))


if __name__ == "__main__":
    main()
