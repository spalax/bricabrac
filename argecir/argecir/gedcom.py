# Copyright Louis Paternault 2023
#
# This file is part of Argecir.
#
# Argecir is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Argecir is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Argecir. If not, see <https://www.gnu.org/licenses/>.

"""Parse GEDCOM files, and represent is as object of custom types defined here.
"""

import dataclasses
import datetime
import functools
import itertools
import operator
import re
import statistics
import string
import typing


class ParseError(Exception):
    """Error while parsing GEDCOM file."""


# First pass: convert lines to tuples of (level, xref, tag, value).
_RE_GEDCOM_LINE = re.compile(
    r"^ *(?P<level>[0-9]+) *(?P<xref>@\S+@)? *(?P<tag>\S+) *(?P<value>.*)$"
)


def _parse1(lines):
    for i, line in enumerate(lines):
        if not line.strip():
            # Ignore empty lines
            continue
        if match := _RE_GEDCOM_LINE.match(line):
            groups = match.groupdict()
            yield {
                "level": int(groups["level"]),
                "xref": groups["xref"] if groups["xref"] else None,
                "tag": groups["tag"],
                "value": groups["value"] if groups["value"] else None,
            }
        else:
            raise ParseError(
                f"Error: Line {i} is not a valid GEDCOM line: {line.strip()}"
            )


# Second pass: convert tuples (as returned by _parse1()) into data structures with level.
# A bit equivalent to indenting the file
@dataclasses.dataclass
class Structure:
    """A GEDCOM piece of data : xref, value, and sub-structures.

    A lot of this stuff is optional.
    """

    tag: str | None
    xref: typing.Optional[str] = None
    value: typing.Optional[str] = None
    children: typing.Sequence[typing.Self] = dataclasses.field(
        default_factory=list, init=False
    )

    def __getitem__(self, key):
        for child in self.children:
            if child.tag.lower() == key.lower():
                return child
        raise KeyError(key)

    def __contains__(self, key):
        for child in self.children:
            if child.tag.lower() == key.lower():
                return True
        return False

    def get(self, key, default):
        """Return `self[key]`. If `key` does not exist, return `default`."""
        try:
            return self[key]
        except KeyError:
            return default

    def iter(self, tag):
        """Iterate over children of tag `tag`."""
        for child in self.children:
            if child.tag.lower() == tag.lower():
                yield child

    def get_date(self, tag, default) -> datetime.date:
        """Return `self[tag]["DATE"]`."""
        try:
            return datetime.datetime.strptime(
                self[tag]["DATE"].value, "%d %b %Y"
            ).date()
        except KeyError:
            return default


def _parse2(lines):
    lastlevel = -1
    last = {-1: Structure(tag="ROOT")}  # Fake structure:
    for line in lines:
        structure = Structure(line["tag"], line["xref"], line["value"])
        for level in range(lastlevel + 1, line["level"]):
            last[level] = Structure(None)
            last[level - 1].children.append(last[level])
        last[line["level"]] = structure
        last[line["level"] - 1].children.append(structure)
        lastlevel = line["level"]
    return last[-1]


# Third pass: convert data got from _parse3() into a tree
_RE_NAME = re.compile(r"^ *(\S*)? */(.*)/ *$")


def _parse_names(text):
    if match := _RE_NAME.match(text):
        return tuple(string.capwords(name) for name in match.groups())
    return (text, "")


@functools.total_ordering
@dataclasses.dataclass
class Individual:
    # pylint: disable=too-many-instance-attributes
    """A person."""
    xref: str
    names: tuple
    birth: typing.Optional[datetime.datetime] = None
    death: typing.Optional[datetime.datetime] = None
    families_down: list = dataclasses.field(init=False, default_factory=list)
    family_up: typing.Optional["Family"] = None
    depth: int = dataclasses.field(init=False, default=0)
    angle: float = dataclasses.field(init=False, default=0)

    def __lt__(self, other):
        if not isinstance(other, Individual):
            raise NotImplementedError
        if isinstance(self, Phantom) and isinstance(other, Phantom):
            return id(self) < id(other)
        if isinstance(self, Phantom):
            return False
        if isinstance(other, Phantom):
            return True
        if self.birth is not None and other.birth is not None:
            return self.birth < other.birth
        if self.death is not None and other.death is not None:
            return self.death < other.death
        return tuple(reversed(self.names)) < tuple(reversed(other.names))

    @property
    def height(self):
        """Return height of `self` in the family tree.

        A person with no children has height 0.
        A parent without grand-children has height 1.
        And so on.
        """
        if self.families_down:
            return max(family.height for family in self.families_down)
        return 0

    @property
    def partners(self):
        """Iterate over partners of `self`."""
        for family in self.families_down:
            for partner in family.partners:
                if partner != self:
                    yield partner

    def siblings(self, *, include_phantoms=False):
        """Iterate over full-siblings of `self` (not half-siblings)."""
        if self.family_up is None:
            return
        for child in self.family_up.children:
            if isinstance(child, Phantom) and not include_phantoms:
                continue
            if child != self:
                yield child

    def enforce_height(self, height, depth):
        """Create phantom person and family so that `self`'s height is height.

        :param int height: Desired height.
        :param int depth: `self`'s depth.

        As a side-effect, this method set this person, and descedant's depth.
        """
        # Set depth
        self.depth = depth
        for partner in self.partners:
            partner.depth = depth

        # Recursively enforce height
        if height == 0:
            return
        if not self.families_down:
            self.families_down.append(Family(xref="", partners=[self]))
        for i, family in enumerate(self.families_down):
            family.enforce_height(height, depth, nth=i, total=len(self.families_down))

    def iter_families(self, height):
        """Iterate over familes of a given height."""
        for family in self.families_down:
            yield from family.iter_families(height)

    def iter_individuals(self, height, *, phantoms=True, partners=True):
        """Iterate over individuals of given height.

        :param int height: Height of individuals.
        :param bool phantoms: If `True`, include phantoms.
        :param bool partners: If `True`, include partnars (persons that are not
            direct descendant of this family, but are married with direct
            descendants).
        """
        if self.height == height:
            if phantoms or not isinstance(self, Phantom):
                yield self
            if partners:
                for partner in self.partners:
                    if phantoms or not isinstance(partner, Phantom):
                        yield partner
        for family in self.families_down:
            yield from family.iter_individuals(
                height, phantoms=phantoms, partners=partners
            )

    @property
    def lifespan(self):
        """Return this person's lifespan, for instance "1902 — 1986" or "1994 —     "."""
        if self.birth is None and self.death is None:
            return ""
        birth = self.birth.year if self.birth is not None else "    "
        death = self.death.year if self.death is not None else "    "
        return f"{birth} — {death}"

    def pprint(self, *, prefix=""):
        """Pretty print family."""
        if not self.families_down:
            if " ".join(self.names):
                print(prefix + self.pprint_names())
        else:
            for family in self.families_down:
                family.pprint(prefix=prefix)

    def pprint_names(self):
        """Pretty print `self` names, with additional information.

        Maybe debug_names() might have been a better name…
        """
        return " ".join(self.names) + f" {self.angle=}"


def _phantom_counter(prefix):
    """Infinitely iterates over `{prefix}0`, `{prefix}1`, etc."""
    for i in itertools.count():
        yield f"{prefix}{i}"


@dataclasses.dataclass
class Phantom(Individual):
    """A phantom person.

    Used to "fill-in" the genealogy tree, to make printing it easier.
    """

    xref: str = dataclasses.field(
        init=False, default_factory=_phantom_counter("PI").__iter__
    )
    names: tuple = ("", "")

    def pprint_names(self):
        return "XXX" + f" {self.angle=}"


@dataclasses.dataclass
class Family:
    """A family: some partners (maybe only one), and some children (maybe none)."""

    xref: str
    partners: typing.Sequence["Individual"] = dataclasses.field(default_factory=list)
    children: typing.Sequence["Individual"] = dataclasses.field(
        init=False, default_factory=list
    )

    def pprint(self, *, prefix=""):
        """Pretty print the family."""
        print(prefix + " + ".join(partner.pprint_names() for partner in self.partners))
        for child in sorted(self.children):
            child.pprint(prefix=prefix + "  ")

    @property
    def height(self):
        """Return this family's height.

        A family with no children has height 0.
        A family with children but no grand-children has height 1.
        And so on…
        """
        if self.children:
            return max(child.height for child in self.children) + 1
        return 0

    def enforce_height(self, height, depth=0, *, nth=0, total=1):
        """Create phantom person and family so that `self`'s height is height.

        Family is the `nth` th family (first, second, third…) of given partner
        out of `total` (counting from 0).
        """
        if nth == 0:
            for _ in range(
                len(self.children)
                + sum(len(list(child.partners)) for child in self.children),
                len(self.partners),
            ):
                self.children.append(Phantom(family_up=self))
        else:
            for _ in range(
                len(self.children)
                + sum(len(list(child.partners)) for child in self.children),
                len(self.partners) - 1,
            ):
                self.children.append(Phantom(family_up=self))
        if total > 1:
            # Parent has several marriage.
            # This family should have at least two children to give more space in the tree.
            for _ in range(len(list(self.stepchildren())), 2):
                self.children.append(Phantom(family_up=self))
        if height > 0:
            for child in self.children:
                child.enforce_height(height - 1, depth + 1)

    def iter_families(self, height):
        """Iterate over familes of a given height."""
        if self.height == height:
            yield self
        for child in sorted(self.children):
            yield from child.iter_families(height)

    def iter_individuals(self, height, *, phantoms=True, partners=True):
        """Iterate over individuals of given height.

        :param int height: Height of individuals.
        :param bool phantoms: If `True`, include phantoms.
        :param bool partners: If `True`, include partnars (persons that are not
            direct descendant of this family, but are married with direct
            descendants).

        Note that this method does not iterate over this family's own partners:
        only descendants.
        """
        for child in sorted(self.children):
            if isinstance(child, Phantom) and not phantoms:
                continue
            yield from child.iter_individuals(
                height, phantoms=phantoms, partners=partners
            )

    def stepchildren(self, *, phantoms=True):
        """Iterate over children AND step-children"""
        for child in sorted(self.children):
            if phantoms or not isinstance(child, Phantom):
                yield child
            for partner in child.partners:
                if phantoms or not isinstance(partner, Phantom):
                    yield partner

    @property
    def realchildren(self):
        """Return a *list* of *real* children."""
        return [
            child for child in sorted(self.children) if not isinstance(child, Phantom)
        ]

    def __iter__(self):
        yield self
        for child in self.children:
            if isinstance(child, Phantom):
                continue
            for family in child.families_down:
                yield from family

    @property
    def angle(self):
        """Return the `angle` of the family, that is, the mean of its partners."""
        return statistics.mean(partner.angle for partner in self.partners)


def _parse3(structures):
    families = {}
    individuals = {}

    # Create data, without references
    for structure in structures.iter("FAM"):
        families[structure.xref] = Family(structure.xref.strip("@"))
    for structure in structures.iter("INDI"):
        individuals[structure.xref] = Individual(
            structure.xref.strip("@"),
            _parse_names(structure["name"].value),
            birth=structure.get_date("birt", None),
            death=structure.get_date("deat", None),
        )

    # Link references
    for structure in structures.iter("FAM"):
        for key in ("WIFE", "HUSB"):
            if key in structure:
                families[structure.xref].partners.append(
                    individuals[structure[key].value]
                )
                individuals[structure[key].value].families_down.append(
                    families[structure.xref]
                )
        for child in structure.iter("chil"):
            families[structure.xref].children.append(individuals[child.value])
            individuals[child.value].family_up = families[structure.xref]

    # Sort children and partners
    for family in families.values():
        family.children.sort()
        family.partners = [
            partner
            for partner in sorted(family.partners)
            if partner.family_up is not None
        ] + [
            partner for partner in sorted(family.partners) if partner.family_up is None
        ]

    return families


# Parse
def parse(texte: str, *, center: str = None) -> Family:
    """Parse a GEDCOM file."""
    families = _parse3(_parse2(_parse1(texte)))
    if center is None:
        for family in sorted(families.values(), key=operator.attrgetter("xref")):
            if (
                len(
                    list(
                        partner
                        for partner in family.partners
                        if partner.family_up is not None
                    )
                )
                == 0
            ):
                return family
        # No center found. Return first family
        return min(families)

    # Argument `center` was given: we look for it.
    if "," in center:
        name, year = center.split(",", 1)
        try:
            year = int(year)
        except ValueError:
            name = center
            year = None
    else:
        name = center
        year = None

    for family in sorted(families.values(), key=operator.attrgetter("xref")):
        for partner in family.partners:
            if " ".join(partner.names).lower() == name.lower():
                if (
                    # We ignore year
                    year
                    is None
                ) or (
                    # We check that the person's birth year is the desired one
                    getattr(partner, "birth", None).year
                    == year
                ):
                    return family

    # Name not found. Raise an helpful exception.
    for family in sorted(families.values(), key=operator.attrgetter("xref")):
        for child in family.stepchildren():
            if " ".join(child.names).lower() == center.lower():
                raise ValueError(
                    f"Name {center} found, but has no children. Cannot build genealogy tree."
                )

    raise ValueError(f"Name {center} not found.")
