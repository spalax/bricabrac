#!/usr/bin/env python

# Copyright Louis Paternault 2023
#
# This file is part of Argecir.
#
# Argecir is free software: you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# Argecir is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# Argecir. If not, see <https://www.gnu.org/licenses/>.

"""Fanfans2GEDCOM

Convertit l'arbre généalogique au format maison en un arbre au format GEDCOM.

Usage : cat FICHIER.csv | python fanfans2gedcom.py

L'arbre au format GEDCOM est affiché sur la sortie standard.

This program can parse the custom-format made by one of my uncles to handle the
family genealogy tree. It is indepedant from the rest of Argecir, and can be
removed if Argecir is ever successful on its own.
"""

import csv
import dataclasses
import datetime
import itertools
import re
import sys
import typing

RE_G = re.compile("g[0-9]+")


@dataclasses.dataclass
class Famille:
    """Une famille (avec un mari et une femme, et des enfants.

    L'arbre généalogique pour lequel je fais ce programme ne contient, pour le
    moment, que des couples hétérosexuels.
    """

    identifiant: int = dataclasses.field(
        default_factory=itertools.count(0).__next__, init=False
    )
    mari: typing.Optional["Individu"] = None
    femme: typing.Optional["Individu"] = None
    enfants: typing.Sequence["Individu"] = dataclasses.field(default_factory=list)


@dataclasses.dataclass
class Individu:
    """Une personne (avec un prénom, un nom, un sexe, etc."""

    # pylint: disable=too-many-instance-attributes
    identifiant: str
    prénom: str
    nom: str
    sexe: str
    naissance: typing.Optional[datetime.date]
    décès: typing.Optional[datetime.date]
    epoux_de: list = dataclasses.field(init=False, default_factory=list)
    enfant_de: typing.Optional[Famille] = None

    def ajoute_époux(self, époux: "Individu"):
        """Ajoute un époux à cette personne, en modifiant la famille en conséquence."""
        if self.sexe == "m":
            famille = Famille(mari=self, femme=époux)
        else:
            famille = Famille(mari=époux, femme=self)
        self.epoux_de.append(famille)
        époux.epoux_de.append(famille)

    def ajoute_parents(self, famille):
        """Ajoute un parent à cet enfant, en modifiant sa famille ne conséquence."""
        self.enfant_de = famille
        famille.enfants.append(self)


_RE_DATE = re.compile(r"(\d{1,2})/(\d{1,2})/(\d+)")


def _date2gedcom(text):
    jour, mois, année = _RE_DATE.match(text).groups()
    return datetime.date(int(année), int(mois), int(jour)).strftime("%d %b %Y")


compteur = itertools.count().__next__


def main():
    """Fonction principale."""
    # pylint: disable=too-many-branches
    individus = {}

    # Lecture du fichier d'entrée
    for ligne in csv.DictReader(sys.stdin.readlines()):
        # On détermine le code unique de la personne
        clefs = list(
            sorted(int(key[1:]) for key in ligne.keys() if RE_G.match(key.strip()))
        )
        identifiant = tuple(
            int(ligne[f"g{clef}"]) for clef in clefs if int(ligne[f"g{clef}"]) != 0
        )

        if identifiant not in individus:
            # La personne n'existe pas encore : on la crée
            individus[identifiant] = Individu(
                identifiant="X".join(str(i) for i in identifiant),
                prénom=ligne["prénom"],
                nom=ligne["nom"],
                sexe=ligne["sexe"],
                naissance=_date2gedcom(ligne["naissance"])
                if ligne["naissance"].strip()
                else None,
                décès=_date2gedcom(ligne["déces"]) if ligne["déces"].strip() else None,
            )

        # La personne est-elle mariée ?
        if ligne["nom conjoint"].strip() or ligne["prénom conjoint"].strip():
            id_partenaire = identifiant + ("c", compteur())
            individus[id_partenaire] = Individu(
                identifiant="X".join(str(i) for i in id_partenaire),
                prénom=ligne["prénom conjoint"].strip()
                if ligne["prénom conjoint"].strip() != "?"
                else "",
                nom=ligne["nom conjoint"].strip()
                if ligne["nom conjoint"].strip() != "?"
                else "",
                # L'arbre généalogique pour lequel je fais ce programme ne
                # contient que des couples hétérosexuels
                sexe={"m": "f", "f": "m"}[ligne["sexe"]],
                naissance=_date2gedcom(ligne["naissance conjoint"])
                if ligne["naissance conjoint"].strip()
                else None,
                décès=_date2gedcom(ligne["décès conjoint"])
                if ligne["décès conjoint"]
                else None,
            )
            individus[identifiant].ajoute_époux(individus[id_partenaire])

        # Ajout de la personne comme enfant d'une famille
        if identifiant and identifiant[:-1] in individus:
            parent = identifiant[:-1]
            if individus[parent].epoux_de:
                famille = individus[parent].epoux_de[-1]
            else:
                # Famille monoparentale : il n'y a pas encore de famille de créée
                if individus[parent].sexe == "m":
                    parents = Famille(mari=individus[parent])
                else:
                    parents = Famille(femme=individus[parent])
                individus[parent].epoux_de.append(parents)
                famille = parents
            if individus[identifiant] not in famille.enfants:
                individus[identifiant].ajoute_parents(famille)

    # Génération de la liste des familles
    familles = []
    for individu in individus.values():
        for famille in individu.epoux_de + [individu.enfant_de]:
            if famille is not None and famille not in familles:
                familles.append(famille)

    # Génération du code GEDCOM
    for individu in individus.values():
        print(f"0 @I{individu.identifiant}@ INDI")
        print(f"1 NAME {individu.prénom} /{individu.nom}/")
        print(f"1 SEX {individu.sexe}")
        if individu.naissance is not None:
            print("1 BIRT")
            print(f"2 DATE {individu.naissance}")
        if individu.décès is not None:
            print("1 DEAT")
            print(f"2 DATE {individu.décès}")
        if individu.enfant_de is not None:
            print(f"1 FAMC @F{individu.enfant_de.identifiant}@")
        for famille in individu.epoux_de:
            print(f"1 FAMS @F{famille.identifiant}@")

    for famille in familles:
        print(f"0 @F{famille.identifiant}@ FAM")
        if famille.mari is not None:
            print(f"1 HUSB @I{famille.mari.identifiant}@")
        if famille.femme is not None:
            print(f"1 WIFE @I{famille.femme.identifiant}@")
        for enfant in famille.enfants:
            print(f"1 CHIL @I{enfant.identifiant}@")


if __name__ == "__main__":
    main()
