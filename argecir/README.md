argecir — Generate circular genealogy trees
===========================================

This program takes a GEDCOM file, and produce a circular genealogy tree,
centered on a given couple of ancestors, with their children, grand-children, etc.

Example
-------

The following command:

~~~
$ python -m argecir example.ged | lualatex
~~~

produces the following file:

[![Circular genealogy tree](example.png)](example.pdf)

⚠️ Warning
==========

This tool is more or less a working draft, and makes a lot of assumptions.

- Error handling is bad. Which means that some issues will produce an unexpected tree instead of warning you that something weird is happening.
- Each child has at most two parents (which means that, for instance, an adopted chil can have biological parents, adoptive parents, but not both).
- I am not sure about how this program handles same-sex marriage (no such marriage in the family I designed this program for).
- Each person can have one partner. Some limited support for two partners (remarriage, for instance) is provided. I do not know what would happen with more than two partners.
- I would be surprised if the whole GEDCOM specification was supported; I do not know which subset of GEDCOM is supported.
- Loops in the genealogy tree are not supported (marriage between cousins, for instance).

Configuration file
==================

When running ``python -m argecir foobar.ged``, the program also looks for an (optional) file ``foobar.toml``, with the following structure:

Note that you can also call argecir on the configuration file: ``python -m argecir foobar.toml``.

~~~toml
file = "-"
template = "argecir.tex"

[layout]
unit = "1mm"
leaf = { width = 25, height = 11 }
radius = [5, 260, 290, 320]
angles = [0, 7, 7]
~~~

- `file`: Which GED file to process. If empty (or "-"), read data from standard input.
- `template`: Which template to use? Can be `argecir.tex` (generates LaTeX code to be compiled with LuaLateX) or `argecir.gv` (generates Graphviz code to be compiled with ``dot``).
- ``layout``:
    - ``unit``: Unit of the other length in this section.
    - ``leaf``: Width, and minimum height of leaves.
    - ``radius``: The first length acts on the radius of the first circle. The second length is the radius of the child leaves; the third one is the radius of the grand-children leaves, and so on.
    - ``angles``: The first number is ignored. The second one is the angle (in degrees) between a person's leaf and their partner's one (relative to the center of each leaf).

Install
-------

It is not (yet?) possible do install this package from [PyPI](https://pypi.org). Instead:

0. Download or clone this repository, and move to the right directory.
0. Run ``python3 setup.py install``
