début — Open a dialog box to choose softwares to run
====================================================

I want some softwares to run when I login to my computer, but sometimes, when I am in a hurry, I do not want to. So:

- the directory ``$HOME/.autorun`` contains softwares to run (mainly symlinks to real binaries);
- I set ``début.sh`` as automatically launched when I login (in Gnome3, KDE, or whatever).

When I login, I see a dialog box with the list of softwares if ``$HOME/.autorun``. I can enable/disable them, and when I click OK, the selected ones are launched.

Usage
-----

~~~sh
$ ./debut.sh
~~~

Install
-------

0. Download or clone this repository, and move to the right directory.
0. Copy the ``debut.sh`` file into a directory of your ``PATH`` environment variable.
