#!/bin/bash

# Copyright 2010-2021 Louis Paternault
#
# Début is free software: you can redistribute it and/or modify
# it under the terms of the GNU Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Début is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Public License for more details.
#
# You should have received a copy of the GNU Public License
# along with Début.  If not, see <http://www.gnu.org/licenses/>.

# Ouvre une boite de dialogue contenant la liste des programmes contenus dans ~/.autorun
# L'utilisateur peut décocher les programmes non désirés. Les programmes cochés sont lancés.
#
# Utilisé pour les fois où je ne veux pas lancer tous mes programmes habituels au démarrage.

AUTORUN_DIR="$HOME/.autorun"
WAIT=2
if [ ! -d $AUTORUN_DIR ] || [ ! -x $AUTORUN_DIR ]
then
  echo "Error: $AUTORUN_DIR does not exist."
  exit 1
fi

cd $AUTORUN_DIR
i=0
string=""
for elem in *
do
  echo $elem | grep -q '^\.' && continue
  echo $elem | grep -q '~$' && continue
  if [ ! -d "$elem" ] && [ -x "$elem" ]
  then
    # Les entrées sont cochées par défaut, sauf si elles commencent par "_"
    value=TRUE
    echo $elem | grep -q '^_' && value=FALSE
    displayed=$(echo $elem | sed 's/^_\?[[:digit:]]*-//')
    string="$string $value $elem $displayed"
  fi
done

apps=$(zenity --list --checklist --text "Choisissez les applications à lancer" --hide-column 2 --column "" --column "" --column "" --title="Démarrage" --multiple --hide-header --separator=" " $string)
cd $HOME
for run in $apps
do
  bin=${AUTORUN_DIR}/${run}
  if [ -L $bin ]
  then
    exec -a $(basename $(readlink $bin)) $bin &
  else
    $bin &
  fi
  sleep $WAIT
done
